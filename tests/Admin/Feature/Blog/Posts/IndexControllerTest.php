<?php

namespace Tests\Admin\Feature\Blog\Posts;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class IndexControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Log in a user with a specific role.
     *
     * @param  $is_admin
     */
    protected function logInUser($is_admin)
    {
        $user = factory(User::class)->create([
            'is_admin' => $is_admin,
        ]);
        $this->be($user);
    }

    /** @test */
    public function posts_are_assigned_to_view()
    {
        $this->logInUser(1);
        $posts = factory(Post::class, 2)->create();

        $response = $this->get('/admin/posts');

        $response->assertSee($posts[0]->title);
        $response->assertSee($posts[1]->title);
    }
}
