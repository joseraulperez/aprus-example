<?php

namespace Tests\Site\Feature\Blog;

use App\Models\Post;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class PostsControllerTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function all_posts_are_assigned_to_view()
    {
        $posts = factory(Post::class, 2)->create([
            'status' => Post::STATUS_ONLINE,
            'locale' => 'en',
        ]);
        $response = $this->get('/blog');

        $response->assertSee($posts[0]->title);
        $response->assertSee($posts[1]->title);
    }
}
