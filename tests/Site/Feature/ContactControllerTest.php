<?php

namespace Tests\Site\Feature;

use App\Mail\ContactForm;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class ContactControllerTest extends TestCase
{
    /**
     * @test
     */
    public function choose_contact_view()
    {
        $response = $this->get('/contact-us');
        $response->assertViewIs('site.contact');
    }

    /**
     * @test
     */
    public function invalid_contact_form_return_to_contact_view_with_errors()
    {
        $response = $this->post('/contact-us');
        $response->assertSessionHasErrors(['name', 'email', 'message']);
        $response->assertStatus(302);
    }

    /**
     * @test
     */
    public function valid_contact_form_send_email_and_return_to_contact_view_with_message()
    {
        Mail::fake();

        $response = $this->post('/contact-us', [
            'name' => 'Fake Name',
            'email' => 'fake@email.com',
            'message' => 'Fake Message',
        ]);

        Mail::assertSent(ContactForm::class, function ($mail){
            return $mail->hasTo('support@aprus.com');
        });

        $response->assertSessionHas('message');
        $response->assertStatus(302);
    }
}
