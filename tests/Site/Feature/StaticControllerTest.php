<?php

namespace Tests\Site\Feature;

use Tests\TestCase;

class StaticControllerTest extends TestCase
{
    public function fakeVisitsProvider()
    {
        return [
            'visit /about-us' => [
                'url' => '/about-us',
                'view' => 'site.static.about',
            ],
            'visit /legal/privacy-policy' => [
                'url' => '/legal/privacy-policy',
                'view' => 'site.static.privacy_policy',
            ],
            'visit /legal/terms-of-service' => [
                'url' => '/legal/terms-of-service',
                'view' => 'site.static.terms_of_service',
            ],
        ];
    }

    /**
     * @test
     * @dataProvider fakeVisitsProvider
     *
     * @param $url
     * @param $view
     */
    public function render_correct_view($url, $view)
    {
        $response = $this->get($url);
        $response->assertViewIs($view);
    }
}
