<?php

namespace Tests\Site\Feature;

use Tests\TestCase;

class HomeControllerTest extends TestCase
{
    /**
     * @test
     */
    public function choose_site_home_view()
    {
        $response = $this->get('/');
        $response->assertViewIs('site.home');
    }
}
