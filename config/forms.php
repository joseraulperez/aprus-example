<?php

return [
    'course' => [
        'title' => 'required',
        'image' => 'required',
        'description' => 'required',
        'position' => 'required',
    ],
    'topic' => [
        'title' => 'required',
        'description' => 'required',
        'position' => 'required',
    ],
    'lesson' => [
        'title' => 'required',
        'position' => 'required',
    ],
    'post' => [
        'title' => 'required',
        'title_slug' => 'required',
        'excerpt' => 'required',
        'content' => 'required',
        'featured_image' => 'required',
        'meta_title' => 'required',
        'meta_description' => 'required',
        'status' => 'required',
    ],
    'phrase' => [
        'text' => 'required',
        'is_vocabulary' => 'required',
    ],
    'step' => [
        'title' => 'required',
        'type' => 'required',
    ],
];