<?php

return [

    'taxes_percentage' => 21,

    'plans' => [
        'annually' => [
            'plan_id' => 'annual-83.88',
            'month_amount' => '6.99',
            'membership_amount' => '83.88',
            'taxes_amount' => '17.61',
            'total_amount' => '101.49',
            'period' => 12,
        ],

        'monthly' => [
            'plan_id' => 'monthly-9.99',
            'month_amount' => '9.99',
            'membership_amount' => '9.99',
            'taxes_amount' => '2.10',
            'total_amount' => '12.09',
            'period' => 1,
        ],
    ]
];
