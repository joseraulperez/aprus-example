<?php

return [
    1 => 'Beginner',
    2 => 'Basic',
    3 => 'Intermediate',
    4 => 'Advance',
];