<?php

return [
    'types' => [
        0 => 'Choose and Write',
        1 => 'Explanation',
        2 => 'Microphone: Listen & Repeat',
        3 => 'Microphone: Translate from native language to Russian	',
        4 => 'Multiple choice',
        5 => 'Write in Russian',
        6 => 'Write image in native language',
        7 => 'Order the phrases',
    ],
    'vue-components' => [
        0 => 'ChooseAndWrite',
        1 => 'Explanation',
        2 => 'ListenAndRepeat',
        3 => 'ReadAndSpeak',
        4 => 'MultipleChoice',
        5 => 'WriteInRussian',
        6 => 'WriteImageInNativeLanguage',
        7 => 'Order the phrases',
    ],
];
