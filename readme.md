# Aprus Example
## Notice that this is NOT the whole source code, it is only an extraction of [aprus.com](https://www.aprus.com)

![Image of Aprus](http://joseraul.es/img/aprus.jpg)

### What can I find?

* Admin (Partial)
    * Posts (CRUD) (/app/Http/Controllers/Admin/Blog/*)
    * Tests (/tests/Admin/Feature/Blog/*)


* App (Partial)
    * Vue components (/resources/app/js/components/*)


* Site (public page) (Partial)
    * Controllers (/app/Http/Controllers/Site/*)
    * Views (/resources/views/site/*)
    * Tests (/tests/Site/Feature/*)


* factories (Partial)


* migrations (All)