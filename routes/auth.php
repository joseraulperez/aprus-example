<?php

/*
|--------------------------------------------------------------------------
| Scaffolding
|--------------------------------------------------------------------------
*/
Auth::routes();


/*
|--------------------------------------------------------------------------
| Verify Email
|--------------------------------------------------------------------------
*/
Route::get('/auth/verify', [
    'uses'      => 'Auth\VerifyEmailController@index',
    'as'        => 'auth.verify_email',
]);

/*
|--------------------------------------------------------------------------
| Register
|--------------------------------------------------------------------------
*/
Route::get('/register/success', [
    'uses'      => 'Auth\RegisterController@success',
    'as'        => 'auth.register.success',
]);


/*
|--------------------------------------------------------------------------
| Social Login
|--------------------------------------------------------------------------
*/
Route::get('/login/{provider}', [
    'uses'      => 'Auth\LoginProviderController@redirectToProvider',
    'as'        => 'login.provider',
    'where'     => [
        'provider' => 'google|facebook|twitter',
    ],
]);
Route::get('/login/{provider}/callback', [
    'uses'      => 'Auth\LoginProviderController@handleProviderCallback',
    'as'        => 'login.provider.callback',
    'where'     => [
        'provider' => 'google|facebook|twitter',
    ],
]);