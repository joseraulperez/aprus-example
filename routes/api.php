<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/*
|--------------------------------------------------------------------------
| App
|--------------------------------------------------------------------------
*/
Route::group([
    'namespace'     => 'App\Api',
    'prefix'        => 'app',
    'middleware'    => [],
], function() {


    /*
    |--------------------------------------------------------------------------
    | Progress
    |--------------------------------------------------------------------------
    */
    Route::post('/progress/save-lesson', [
        'uses'      => 'Progress\LessonController@index',
        'as'        => 'api.progress.save_lesson',
    ]);

    Route::post('/progress/save-topic', [
        'uses'      => 'Progress\TopicController@index',
        'as'        => 'api.progress.save_topic',
    ]);

    Route::post('/speech-recognition', [
        'uses'      => 'SpeechRecognitionController@index',
    ]);

    // Stripe
    Route::post(
        '/stripe/webhook',
        '\Laravel\Cashier\Http\Controllers\WebhookController@handleWebhook'
    );


    /*
    |--------------------------------------------------------------------------
    | Chat
    |--------------------------------------------------------------------------
    */
    Route::get('/chat/{user_id}', [
        'uses'      => 'Chat\GetController@index',
        'as'        => 'api.chat.get',
    ]);

    Route::post('/chat/{user_id}', [
        'uses'      => 'Chat\PostController@index',
        'as'        => 'api.chat.post',
    ]);

});
