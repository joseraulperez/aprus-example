<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::group([
    'namespace'     => 'Admin',
    'prefix'        => 'admin',
    'middleware'    => 'admin',
], function(){

    Route::get('/', [
        'uses'      => 'HomeController@index',
        'as'        => 'admin.home',
    ]);

    Route::get('/import', [
        'uses'      => 'ImportController@import',
    ]);

    /*
    |--------------------------------------------------------------------------
    | Posts
    |--------------------------------------------------------------------------
    */
    Route::group(['namespace'=>'Blog\Posts', 'prefix'=>'posts'], function() {

        // Index
        Route::get('/', [
            'uses'      => 'IndexController@index',
            'as'        => 'admin.posts.index',
        ]);

        // Create
        Route::get('/create', [
            'uses'      => 'CreateController@index',
            'as'        => 'admin.posts.create',
        ]);
        Route::post('/create', [
            'uses'      => 'CreateController@store',
        ]);

        // Edit
        Route::get('/{post_id}', [
            'uses'      => 'EditController@index',
            'as'        => 'admin.posts.edit',
        ]);
        Route::post('/{post_id}', [
            'uses'      => 'EditController@update',
        ]);

        // Delete
        Route::get('/{post_id}/delete', [
            'uses'      => 'DeleteController@index',
            'as'        => 'admin.posts.delete',
        ]);

    });

    /*
    |--------------------------------------------------------------------------
    | Phrases
    |--------------------------------------------------------------------------
    */

    // Index
    Route::get('/phrases', [
        'uses'      => 'Phrases\IndexController@index',
        'as'        => 'admin.phrases.index',
    ]);

    // Create
    Route::get('/phrases/create', [
        'uses'      => 'Phrases\CreateController@index',
        'as'        => 'admin.phrases.create',
    ]);
    Route::post('/phrases/create', [
        'uses'      => 'Phrases\CreateController@store',
    ]);

    // Edit
    Route::get('/phrases/{phrase_id}', [
        'uses'      => 'Phrases\EditController@index',
        'as'        => 'admin.phrases.edit',
    ]);
    Route::post('/phrases/{phrase_id}', [
        'uses'      => 'Phrases\EditController@update',
    ]);

    // Delete
    Route::get('/phrases/{phrase_id}/delete', [
        'uses'      => 'Phrases\DeleteController@index',
        'as'        => 'admin.phrases.delete',
    ]);


    /*
    |--------------------------------------------------------------------------
    | Courses
    |--------------------------------------------------------------------------
    */

    // Index
    Route::get('/courses', [
        'uses'      => 'Courses\IndexController@index',
        'as'        => 'admin.courses.index',
    ]);

    // Create
    Route::get('/courses/create', [
        'uses'      => 'Courses\CreateController@index',
        'as'        => 'admin.courses.create',
    ]);
    Route::post('/courses/create', [
        'uses'      => 'Courses\CreateController@store',
    ]);

    // Edit
    Route::get('/courses/{course_id}', [
        'uses'      => 'Courses\EditController@index',
        'as'        => 'admin.courses.edit',
    ]);
    Route::post('/courses/{course_id}', [
        'uses'      => 'Courses\EditController@update',
    ]);

    // Delete
    Route::get('/courses/{course_id}/delete', [
        'uses'      => 'Courses\DeleteController@index',
        'as'        => 'admin.courses.delete',
    ]);


    /*
    |--------------------------------------------------------------------------
    | Videos
    |--------------------------------------------------------------------------
    */

    // Index
    Route::get('/videos', [
        'uses'      => 'Videos\IndexController@index',
        'as'        => 'admin.videos.index',
    ]);

    // Create
    Route::get('/videos/create', [
        'uses'      => 'Videos\CreateController@index',
        'as'        => 'admin.videos.create',
    ]);
    Route::post('/videos/create', [
        'uses'      => 'Videos\CreateController@store',
    ]);

    // Edit
    Route::get('/videos/{video_id}', [
        'uses'      => 'Videos\EditController@index',
        'as'        => 'admin.videos.edit',
    ]);
    Route::post('/videos/{video_id}', [
        'uses'      => 'Videos\EditController@update',
    ]);

    // Delete
    Route::get('/videos/{video_id}/delete', [
        'uses'      => 'Videos\DeleteController@index',
        'as'        => 'admin.videos.delete',
    ]);

    /*
    |--------------------------------------------------------------------------
    | Topics
    |--------------------------------------------------------------------------
    */
    Route::get('/courses/{course_id}/topics', [
        'uses'      => 'Topics\IndexController@index',
        'as'        => 'admin.topics.index',
    ]);

    Route::get('/courses/{course_id}/topics/create', [
        'uses'      => 'Topics\CreateController@index',
        'as'        => 'admin.topics.create',
    ]);
    Route::post('/courses/{course_id}/topics/create', [
        'uses'      => 'Topics\CreateController@store',
    ]);

    Route::get('/courses/{course_id}/topics/{topic_id}', [
        'uses'      => 'Topics\EditController@index',
        'as'        => 'admin.topics.edit',
    ]);
    Route::post('/courses/{course_id}/topics/{topic_id}', [
        'uses'      => 'Topics\EditController@update',
    ]);

    Route::get('/courses/{course_id}/topics/{topic_id}/delete', [
        'uses'      => 'Topics\DeleteController@index',
        'as'        => 'admin.topics.delete',
    ]);


    /*
    |--------------------------------------------------------------------------
    | Lessons
    |--------------------------------------------------------------------------
    */
    Route::get('/courses/{course_id}/topics/{topic_id}/lessons', [
        'uses'      => 'Lessons\IndexController@index',
        'as'        => 'admin.lessons.index',
    ]);

    Route::get('/courses/{course_id}/topics/{topic_id}/lessons/create', [
        'uses'      => 'Lessons\CreateController@index',
        'as'        => 'admin.lessons.create',
    ]);
    Route::post('/courses/{course_id}/topics/{topic_id}/lessons/create', [
        'uses'      => 'Lessons\CreateController@store',
    ]);

    Route::get('/courses/{course_id}/topics/{topic_id}/lessons/{lesson_id}', [
        'uses'      => 'Lessons\EditController@index',
        'as'        => 'admin.lessons.edit',
    ]);
    Route::post('/courses/{course_id}/topics/{topic_id}/lessons/{lesson_id}', [
        'uses'      => 'Lessons\EditController@update',
    ]);

    Route::get('/courses/{course_id}/topics/{topic_id}/lessons/{lesson_id}/delete', [
        'uses'      => 'Lessons\DeleteController@index',
        'as'        => 'admin.lessons.delete',
    ]);

    /*
    |--------------------------------------------------------------------------
    | Steps
    |--------------------------------------------------------------------------
    */
    Route::get('/courses/{course_id}/topics/{topic_id}/lessons/{lesson_id}/steps', [
        'uses'      => 'Steps\IndexController@index',
        'as'        => 'admin.steps.index',
    ]);

    Route::get('/courses/{course_id}/topics/{topic_id}/lessons/{lesson_id}/steps/create', [
        'uses'      => 'Steps\CreateController@index',
        'as'        => 'admin.steps.create',
    ]);
    Route::post('/courses/{course_id}/topics/{topic_id}/lessons/{lesson_id}/steps/create', [
        'uses'      => 'Steps\CreateController@store',
    ]);

    Route::get('/courses/{course_id}/topics/{topic_id}/lessons/{lesson_id}/steps/{step_id}', [
        'uses'      => 'Steps\EditController@index',
        'as'        => 'admin.steps.edit',
    ]);
    Route::post('/courses/{course_id}/topics/{topic_id}/lessons/{lesson_id}/steps/{step_id}', [
        'uses'      => 'Steps\EditController@update',
    ]);

    Route::get('/courses/{course_id}/topics/{topic_id}/lessons/{lesson_id}/steps/{step_id}/delete', [
        'uses'      => 'Steps\DeleteController@index',
        'as'        => 'admin.steps.delete',
    ]);

    /*
    |--------------------------------------------------------------------------
    | Users
    |--------------------------------------------------------------------------
    */
    Route::get('/users', [
        'uses'      => 'Users\IndexController@index',
        'as'        => 'admin.users.index',
    ]);

    /*
    |--------------------------------------------------------------------------
    | Chat
    |--------------------------------------------------------------------------
    */
    Route::get('/chat', [
        'uses'      => 'Messages\IndexController@index',
        'as'        => 'admin.chat',
    ]);

    Route::get('/chat/{user_id}', [
        'uses'      => 'Messages\ChatController@index',
        'as'        => 'admin.chat.index',
    ]);
    Route::post('/chat/{user_id}', [
        'uses'      => 'Messages\ChatController@post',
    ]);

    Route::get('/chat/{user_id}/messages/{message_id}/unread', [
        'uses'      => 'Messages\StatusController@unread',
        'as'        => 'admin.chat.unread',
    ]);
    Route::get('/chat/{user_id}/messages/{message_id}/read', [
        'uses'      => 'Messages\StatusController@read',
        'as'        => 'admin.chat.read',
    ]);

    /*
    |--------------------------------------------------------------------------
    | Logs
    |--------------------------------------------------------------------------
    */
    Route::get('logs', [
        'uses'      => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index',
        'as'        => 'admin.logs',
    ]);

    /*
    |--------------------------------------------------------------------------
    | Emails
    |--------------------------------------------------------------------------
    */
    Route::get('/emails', [
        'uses'      => 'Emails\IndexController@index',
        'as'        => 'admin.emails.index',
    ]);

    Route::get('/emails/{email_key}', [
        'uses'      => 'Emails\IndexController@email',
        'as'        => 'admin.emails.email',
    ]);

});