<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace'=>'Site'], function(){

    Route::get('/', [
        'uses'      => 'HomeController@index',
        'as'        => 'site.home',
    ]);

    /*
    |--------------------------------------------------------------------------
    | Static
    |--------------------------------------------------------------------------
    */
    Route::get('/legal/privacy-policy', [
        'uses'      => 'StaticController@privacyPolicy',
        'as'        => 'site.privacy_policy'
    ]);

    Route::get('/legal/terms-of-service', [
        'uses'      => 'StaticController@termsOfService',
        'as'        => 'site.terms_of_service'
    ]);

    Route::get('/about-us', [
        'uses'      => 'StaticController@about',
        'as'        => 'site.about_us',
    ]);

    /*
    |--------------------------------------------------------------------------
    | Contact
    |--------------------------------------------------------------------------
    */
    Route::get('/contact-us', [
        'uses'      => 'ContactController@index',
        'as'        => 'site.contact'
    ]);
    Route::post('/contact-us', [
        'uses'      => 'ContactController@post',
        'as'        => 'site.contact'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Blog
    |--------------------------------------------------------------------------
    */
    Route::get('/blog', [
        'uses'      => 'Blog\PostsController@index',
        'as'        => 'site.blog'
    ]);
    Route::get('/blog/{title_slug}', [
        'uses'      => 'Blog\PostController@index',
        'as'        => 'site.blog.post',
        'where'     => [
            'title_slug' => '[0-9a-z\-]+',
        ],
    ]);


});
