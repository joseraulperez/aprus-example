<?php

/*
|--------------------------------------------------------------------------
| App Routes
|--------------------------------------------------------------------------
*/

Route::group([
    'namespace'     => 'App',
    'prefix'        => 'app',
    'middleware'    => ['auth', 'unread_messages', 'redirect_to_locale'],
], function(){

    /*
    |--------------------------------------------------------------------------
    | Home
    |--------------------------------------------------------------------------
    */
    Route::get('/', [
        'uses'      => 'CoursesController@index',
        'as'        => 'app.home',
    ]);

    /*
    |--------------------------------------------------------------------------
    | Videos
    |--------------------------------------------------------------------------
    */
    Route::get('/videos', [
        'uses'      => 'Videos\IndexController@index',
        'as'        => 'app.videos',
    ]);
    Route::get('/videos/{id}', [
        'uses'      => 'Videos\ItemController@index',
        'as'        => 'app.videos.item',
    ]);

    /*
    |--------------------------------------------------------------------------
    | Courses
    |--------------------------------------------------------------------------
    */
/*    Route::get('/courses', [
        'uses'      => 'CoursesController@index',
        'as'        => 'app.courses',
    ]);*/
    Route::get('/courses/{id}', [
        'uses'      => 'Course\LessonsController@index',
        'as'        => 'app.course',
    ]);
    Route::get('/courses/{id}/vocabulary', [
        'uses'      => 'Course\VocabularyController@index',
        'as'        => 'app.course.vocabulary',
    ]);

    // Lesson
    Route::get('/courses/{course_id}/topic/{topic_id}/lesson/{lesson_id}', [
        'uses'      => 'Course\LessonController@index',
        'as'        => 'app.lesson',
    ]);

    // Test
    Route::get('/courses/{course_id}/topic/{topic_id}/test', [
        'uses'      => 'Course\TestController@index',
        'as'        => 'app.test',
    ]);

    /*
    |--------------------------------------------------------------------------
    | Premium
    |--------------------------------------------------------------------------
    */
    Route::get('/premium', [
        'uses'      => 'Premium\LandingController@index',
        'as'        => 'app.premium.landing',
    ]);
    Route::get('/premium/{plan}', [
        'uses'      => 'Premium\PaymentController@index',
        'as'        => 'app.premium.payment',
        'where'     => [
            'plan' => 'annually|monthly',
        ],
    ]);
    Route::post('/premium/{plan}', [
        'uses'      => 'Premium\PaymentController@post',
        'where'     => [
            'plan' => 'annually|monthly',
        ],
    ]);
    Route::get('/premium/success', [
        'uses'      => 'Premium\PaymentController@success',
        'as'        => 'app.premium.payment.success',
    ]);
    Route::get('/premium/error', [
        'uses'      => 'Premium\PaymentController@error',
        'as'        => 'app.premium.payment.error',
    ]);

    /*
    |--------------------------------------------------------------------------
    | Settings
    |--------------------------------------------------------------------------
    */
    Route::get('/settings/account', [
        'uses'      => 'User\Settings\AccountController@index',
        'as'        => 'app.settings.account',
    ]);
    Route::post('/settings/account', [
        'uses'      => 'User\Settings\AccountController@post',
    ]);

    Route::get('/settings/premium', [
        'uses'      => 'User\Settings\PremiumController@index',
        'as'        => 'app.settings.premium',
    ]);

    Route::get('/settings/subscription', [
        'uses'      => 'User\Settings\Subscription\IndexController@index',
        'as'        => 'app.settings.subscription',
    ]);

    Route::get('/settings/subscription/invoice/{id}', [
        'uses'      => 'User\Settings\Subscription\InvoiceController@index',
        'as'        => 'app.settings.subscription.invoice',
    ]);

    Route::get('/settings/subscription/cancel', [
        'uses'      => 'User\Settings\Subscription\CancelController@index',
        'as'        => 'app.settings.subscription.cancel',
    ]);
    Route::post('/settings/subscription/cancel', [
        'uses'      => 'User\Settings\Subscription\CancelController@post',
    ]);

});