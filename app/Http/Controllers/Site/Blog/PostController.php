<?php

namespace App\Http\Controllers\Site\Blog;

use App\Http\Controllers\Controller;
use App\Models\Post;

class PostController extends Controller
{
    /**
     * Show a post taking into account 'locale' and 'title_slug'.
     * Fail if it doesn't exists.
     *
     * @param $post_model
     * @param $title_slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Post $post_model, $title_slug)
    {
        $locale = app()->getLocale();

        $post = $post_model->where('locale', $locale)
            ->where('title_slug', $title_slug)
            ->firstOrFail();

        return view('site.blog.post', [
            'post' => $post,
        ]);
    }
}
