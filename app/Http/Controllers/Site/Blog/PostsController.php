<?php

namespace App\Http\Controllers\Site\Blog;

use App\Http\Controllers\Controller;
use App\Models\Post;

class PostsController extends Controller
{
    /**
     * Show posts taking into account 'locale' and 'status'.
     *
     * @param $post_model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Post $post_model)
    {
        $locale = app()->getLocale();

        $posts = $post_model->where('locale', $locale)
            ->where('status', Post::STATUS_ONLINE)
            ->orderBy('created_at')
            ->get();

        return view('site.blog.posts', [
            'posts' => $posts,
        ]);
    }
}
