<?php namespace App\Http\Controllers\Site;

use App\Mail\ContactForm;
use Illuminate\Http\Request;
use Mail;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
     * Show contact form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function index()
    {
		return view('site.contact', []);
	}

    /**
     * Send e-mail to 'support@aprus.com' if all parameters are right.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
	public function post(Request $request)
	{
		$this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
        ]);

		$data = $request->all();

        Mail::to('support@aprus.com')
            ->send(new ContactForm($data['name'], $data['email'], $data['message']));

        return redirect()
            ->back()
            ->with('message', [
                'type' => 'success',
                'content' => trans('site/contact.Message sent successfully'),
            ]);
	}
}
