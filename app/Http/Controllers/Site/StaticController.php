<?php namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

class StaticController extends Controller
{
    /**
     * Render privacy_policy.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function privacyPolicy()
    {
        return view('site.static.privacy_policy');
    }

    /**
     * Render terms_of_service.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function termsOfService()
    {
        return view('site.static.terms_of_service');
    }

    /**
     * Render about.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about()
    {
        return view('site.static.about');
    }
}
