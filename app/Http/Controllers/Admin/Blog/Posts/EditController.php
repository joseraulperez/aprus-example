<?php

namespace App\Http\Controllers\Admin\Blog\Posts;

use App\Http\Controllers\Controller;
use App\Models\Post;

class EditController extends Controller
{
    /**
     * Show & fill the Post form.
     *
     * @param Post $post_model
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Post $post_model, $id)
    {
        $post = $post_model->findOrFail($id);

        return view('admin.posts.edit', [
            'post' => $post,
            'locales' => config('locales'),
            'status' => [Post::STATUS_DRAFT => 'Draft', Post::STATUS_ONLINE => 'Online'],
        ]);
    }

    /**
     * Update the post if pass the rules.
     *
     * @param Post $post_model
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Post $post_model, $id)
    {
        $this->validate(request(), config('forms.post'));

        $post = $post_model->findOrFail($id);
        $data = request()->all();

        $post->fill($data);

        try {
            $post->save();
        } catch(\Exception $e) {
            return redirect()
                ->back()
                ->withInput($data)
                ->with('message', [
                    'level' => 'danger',
                    'text' => $e->getMessage(),
                ]);
        }

        return redirect()
            ->route('admin.posts.edit', [$id])
            ->with('message', [
                'level' => 'success',
                'text' => 'Post Updated Successfully',
            ]);
    }
}
