<?php

namespace App\Http\Controllers\Admin\Blog\Posts;

use App\Http\Controllers\Controller;
use App\Models\Post;

class DeleteController extends Controller
{
    /**
     * Delete a post.
     *
     * @param Post $post_model
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index(Post $post_model, $id)
    {
        try {
            $post_model->findOrFail($id)
                ->delete();

        } catch(\Exception $e) {
            return redirect()
                ->back()
                ->with('message', [
                    'level' => 'danger',
                    'text' => $e->getMessage(),
                ]);
        }

        return redirect()
            ->route('admin.posts.index')
            ->with('message', [
                'level' => 'success',
                'text' => 'Post Removed Successfully',
            ]);
    }
}
