<?php

namespace App\Http\Controllers\Admin\Blog\Posts;

use App\Http\Controllers\Controller;
use App\Models\Post;

class CreateController extends Controller
{
    /**
     * Show the post form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.posts.create', [
            'locales' => config('locales'),
            'status' => [Post::STATUS_DRAFT => 'Draft', Post::STATUS_ONLINE => 'Online'],
        ]);
    }

    /**
     * Store a post if the form pass the rules.
     *
     * @param Post $post_model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Post $post_model)
    {
        $this->validate(request(), config('forms.post'));

        $data = request()->all();

        try {
            $post_model->create($data);
        } catch(\Exception $e) {
            return redirect()
                ->back()
                ->withInput($data)
                ->with('message', [
                    'level' => 'danger',
                    'text' => $e->getMessage(),
                ]);
        }

        return redirect()
            ->route('admin.posts.index')
            ->with('message', [
                'level' => 'success',
                'text' => 'Post Created Successfully',
            ]);
    }
}
