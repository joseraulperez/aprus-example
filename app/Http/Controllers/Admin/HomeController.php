<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Message;

class HomeController extends Controller
{
    public function index(Message $message)
    {
        $pending_messages = $message->getPendingMessagesForTeachers();

        return view('admin.home', [
            'pending_messages' => $pending_messages->count(),
        ]);
    }
}
