<?php

namespace App\Http\Middleware;

use Closure;

class RedirectToLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_locale = auth()->user()->locale;
        $current_locale = app()->getLocale();

        if ($user_locale !== $current_locale) {
            return redirect(urls()->getAppHome($user_locale));
        }

        return $next($request);
    }
}
