<?php

namespace App\Http\Middleware;

use App\Models\Message;
use Closure;

class SetUnreadMessages
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();

        $messages = Message::where('receiver_user_id', $user->id)
            ->where('is_read', 0);

        view()->share('unread_messages', $messages->count());

        return $next($request);
    }
}
