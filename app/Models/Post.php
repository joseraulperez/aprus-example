<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    const STATUS_DRAFT = 0;
    const STATUS_ONLINE = 1;

    protected $guarded = ['id'];
}