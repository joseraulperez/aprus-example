<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Post::class, function (Faker $faker) {
    $title = $faker->sentence;

    return [
        'old_id' => $faker->uuid,
        'meta_title' => $title,
        'meta_description' => $faker->sentence,
        'meta_keywords' => null,
        'locale' => $faker->randomElements(['es', 'en'])[0],
        'title' => $faker->sentence,
        'title_slug' => str_slug($title),
        'excerpt' => $faker->sentence,
        'featured_image' => $faker->image(null, 840, 420),
        'status' => $faker->randomElements([\App\Models\Post::STATUS_DRAFT, \App\Models\Post::STATUS_ONLINE])[0],
        'content' => $faker->paragraphs(10, true),
        'category_id' => null,
        'author_id' => null,
        'has_try_for_free_button' => $faker->randomElements([0, 1])[0],
    ];
});
