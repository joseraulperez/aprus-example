<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Topic::class, function (Faker $faker) {

    return [
        'title' => $faker->sentence,
        'description' => $faker->paragraph(1),
        'position' => $faker->numberBetween(1, 10),
        'course_id' => App\Models\Course::inRandomOrder()->get()[0]->id,
        'letters_learnt' => $faker->numberBetween(10, 20),
        'words_learnt' => $faker->numberBetween(10, 20),
    ];
});
