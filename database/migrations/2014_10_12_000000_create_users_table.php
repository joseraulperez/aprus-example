<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
            $table->string('avatar', 255)->nullable();
            $table->string('locale', 2)->nullable();
            $table->timestamp('last_login_at')->nullable();
            $table->boolean('is_admin')->default(0);
            $table->boolean('is_teacher')->default(0);
            $table->boolean('is_confirmed')->default(0);
            $table->string('confirmation_code')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        User::create([
            'name' => 'Elena Markina',
            'email' => 'elena.markina@aprus.com',
            'password' => bcrypt(123456),
            'is_teacher' => 1,
            'locale' => 'es',
            'avatar' => 'https://res.cloudinary.com/aprus/image/upload/v1499342150/shared/lena-avatar.png',
            'is_admin' => 1,
        ]);
        User::create([
            'name' => 'Jose Raul Perez',
            'email' => 'joseraul.perez@aprus.com',
            'password' => bcrypt(123456),
            'is_teacher' => 0,
            'locale' => 'es',
            'avatar' => null,
            'is_admin' => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
