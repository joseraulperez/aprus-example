<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('old_id')->nullable();
            $table->string('meta_title', 255);
            $table->string('meta_description', 255);
            $table->string('meta_keywords')->nullable();
            $table->string('locale');
            $table->string('title', 255);
            $table->string('title_slug', 255);
            $table->string('excerpt', 255);
            $table->string('featured_image', 255);
            $table->unsignedTinyInteger('status');
            $table->text('content');
            $table->unsignedInteger('category_id')->nullable();
            $table->unsignedInteger('author_id')->nullable();
            $table->boolean('has_try_for_free_button')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
