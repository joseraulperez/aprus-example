<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StepOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('step_options', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->unsignedInteger('step_id');
            $table->unsignedInteger('phrase_id');
            $table->timestamps();

            $table->unique(['step_id', 'phrase_id']);
            $table->foreign('step_id')->references('id')->on('steps')->onDelete('cascade');
            $table->foreign('phrase_id')->references('id')->on('phrases')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('step_options');

    }
}
