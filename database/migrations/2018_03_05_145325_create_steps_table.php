<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('steps', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->uuid('old_id')->nullable();
            $table->string('title');
            $table->string('subtitle')->nullable();
            $table->text('text')->nullable();
            $table->string('image')->nullable();
            $table->string('audio', 500)->nullable();
            $table->tinyInteger('position')->unsigned();
            $table->unsignedInteger('lesson_id');
            $table->unsignedSmallInteger('type');
            $table->boolean('is_exam')->default(0);
            $table->boolean('is_sound_option')->default(0);
            $table->string('video')->nullable();
            $table->unsignedSmallInteger('video_start')->nullable();
            $table->unsignedSmallInteger('video_end')->nullable();
            $table->timestamps();

            $table->foreign('lesson_id')->references('id')->on('lessons')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('steps');
    }
}
