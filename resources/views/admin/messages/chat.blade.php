@extends('admin.layouts.main')

@section('content')
    <div class="container">
        @foreach($messages as $message)
            <div class="media">
                <div class="mr-3">
                    <img width="68px" class="img-thumbnail" src="{{ $message->sender->avatar }}"> <br>
                    {{ $message->sender->name }} <br>
                    {{ $message->created_at->diffForHumans() }}
                </div>
                <div class="media-body">
                    <div class="card mb-4 {{ ($message->sender->is_teacher) ? 'border-info' : '' }}">
                        <div class="card-body">
                            {{ $message->content }}
                        </div>
                        <div class="card-footer">
                            <small>Is Read: {{ $message->is_read }}</small>
                        </div>
                    </div>
                </div>
                <div class="ml-3">
                    @if(!$message->sender->is_teacher)
                        <a class="btn btn-light mb-2" href="{{ route('admin.chat.read', [$user_id, $message->id]) }}">Mark as Read</a> <br>
                        <a class="btn btn-light" href="{{ route('admin.chat.unread', [$user_id, $message->id]) }}">Mark as Unread</a>
                    @else
                        <a class="btn btn-light mb-2" >Edit</a> <br>
                    @endif
                </div>
            </div>
        @endforeach

        <hr>

        {!! Form::open() !!}
        <div class="form-group">
            {!! Form::label('content', 'Answer') !!}
            {!! Form::textarea('content', null, ['class' => 'form-control', 'rows' =>'2']) !!}
        </div>
        <div class="form-group">
            <button class="btn btn-primary">Send</button>
        </div>
        {!! Form::close() !!}

    </div>
@endsection