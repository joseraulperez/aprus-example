@extends('admin.layouts.main')

@section('content')
    <div class="container">

        <div class="d-flex  justify-content-between mb-3">
            {!! Form::open(['class' => 'form-inline', 'method' => 'get']) !!}
                {!! Form::select('filters[status]', [0 => 'All', 1 => 'Pending'], null, ['class' => 'form-control mr-3']) !!}
                {!! Form::submit('Filter', ['class'=>'btn btn-light']) !!}
            {!! Form::close() !!}
        </div>

        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Action</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>
                        <a class="btn btn-outline-primary" href="{{ route('admin.chat.index', [$user->id]) }}">See Chat</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection