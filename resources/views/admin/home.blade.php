@extends('admin.layouts.main')

@section('content')
    <div class="container">
        <div class="row mb-4">
            <div class="col-3">
                <div class="card">
                    <div class="card-header">Pending Messages</div>
                    <div class="card-body text-center">
                        <h1>{{ $pending_messages }}</h1>
                    </div>
                </div>
            </div>
        </div>


        <a class="btn btn-primary" href="/admin/import">Import</a>
    </div>
@endsection