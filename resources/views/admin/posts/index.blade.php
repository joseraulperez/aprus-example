@extends('admin.layouts.main')

@section('content')
    <div class="container">

        <div class="d-flex  justify-content-between mb-3">
            {!! Form::open(['class' => 'form-inline', 'method' => 'get']) !!}
                {!! Form::submit('Filter', ['class'=>'btn btn-light']) !!}
            {!! Form::close() !!}

            <a class="btn btn-primary" href="{{ route('admin.posts.create') }}">New Post</a>
        </div>

        <table class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Status</th>
                <th>Locale</th>
                <th>Title</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($posts as $post)
            <tr>
                <td>{{ $post->id }}</td>
                <td>
                    @if (\App\Models\Post::STATUS_DRAFT === $post->status)
                        <span class="badge badge-secondary">Draft</span>
                    @else
                        <span class="badge badge-success">Online</span>
                    @endif
                </td>
                <td>{{ $post->locale }}</td>
                <td>{{ $post->title }}</td>
                <td><a class="btn btn-outline-primary" href="{{ route('admin.posts.edit', [$post->id]) }}">Edit</a></td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <script>
        $(".delete_button").click(function(){
            if(!confirm("Are you sure you want to delete this?")){
                return false;
            }
        });
    </script>
@endsection