@extends('site.layouts.main')

@section('meta_title'){{trans('metas.about_us_title')}}@endsection
@section('meta_description'){{trans('metas.about_us_description')}}@endsection
@section('body_class'){{ 'about' }}@endsection

@section('meta_languages')
    <link rel="alternate" hreflang="en" href="https://www.aprus.com/about-us" />
    <link rel="alternate" hreflang="es" href="https://es.aprus.com/about-us" />
@endsection

@section('content')
    <div class="container text-center mb-4">
        <div class="row d-flex justify-content-center">
            <div class="col-md-8">
                <h2><strong>{{ trans('site/about.title') }}</strong></h2>
                <p class="lead text-muted">{{ trans('site/about.subtitle') }}</p>
            </div>
        </div>
    </div>

    <div class="container mb-5">

        <div class="row d-flex justify-content-center">
            <div class="col-md-4">
                <div class="card box-shadow mb-4">
                    <div class="card-body text-center">
                        <img class="mb-3" src="https://res.cloudinary.com/aprus/image/upload/v1501663774/shared/lena-about.jpg">
                        <h4 class="mb-0"><strong>Elena Markina</strong></h4>
                        <p class="text-muted lead mb-3">{{ trans('site/about.Founder-girl') }}</p>
                        <p>{{ trans('site/about.lena_bio') }}</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card box-shadow">
                    <div class="card-body text-center">
                        <img class="mb-3" src="https://res.cloudinary.com/aprus/image/upload/v1501663774/shared/raul-about.jpg">
                        <h4 class="mb-0"><strong>Jose Raul Perez</strong></h4>
                        <p class="text-muted lead mb-3">{{ trans('site/about.Founder') }}</p>
                        <p>{{ trans('site/about.raul_bio') }}</p>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <div class="text-center mb-5">
        <h4 class="mb-3"><strong>{{ trans('site/about.contact_us_title') }}</strong></h4>
        <div class="button-wrapper text-center">
            <a class="btn btn-lg btn-try-it-free btn-primary" href="{{ route('site.contact') }}">{{trans('site/about.btn_contact')}}</a>
        </div>
    </div>

@endsection

