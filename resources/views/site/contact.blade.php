@extends('site.layouts.main')

@section('meta_title'){{trans('metas.contact_us_title')}}@endsection
@section('meta_description'){{trans('metas.contact_us_description')}}@endsection

@section('meta_languages')
    <link rel="alternate" hreflang="en" href="https://www.aprus.com/contact-us"/>
    <link rel="alternate" hreflang="es" href="https://es.aprus.com/contact-us"/>
@endsection

@section('content')

    <div class="container">

        <div class="row d-flex justify-content-center mb-5">
            <div class="col-md-6 text-center">
                <div class="card">
                    <div class="card-body">

                        <img class="icon icons8-Mailbox-With-Letter"
                             src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAGqklEQVR4Xu2cXWxURRTH/1O6W/ux2/JNBcMWWtNIkRYIaIAIIjG8yEdIzPqi8c0WEojGRB+kPPokJLbEJ+GJ8KKQGI1RqQKJlAhtpMRqqlwELKbEbnf7AW3pmFm6y+6y7c7cmbv3Qs8+Ndlz/jP3/5tz7tnubRno5aoDzNXVaXEQAJcPAQEgAC474PLyVAEEwGUHXF6eKoAAOOsA3wSusgI7l9/J8ImvAAKgcvwciCUADpiqIkkAVNxyIJYAOGCqiiQBUHHLgVgC4ICpKpIEQMUtB2IzAeR7zs91STPucwAByHUkDL9PFWDYUFU5ApDiWOjT6A4w7ARjIXBUMIZ6VUNV4/86EUhLWRaOqUok4zlHJxgi4NwCxylrb/C0bbHJRMfvAaGjIyFMjB0EsJMxVqG7YdV8kwAy1+acRwCcwmjggHWAiZ+VX44CCB2NHcQE3++G8QknnASQWCMOooAdtt4JHFIl4AiA0Ce8Av7Btny0mFwXnA8AD0GgE6NlW1SqwTiAUMtgPTDRlu3Ujw8PYbS3F2N3+jAxPIz70YH43mcFy1FQUgLfvPnwV1aisKQ0l6/S7+cTgNjUg7ZUsMVqKuuU2aRRAA9OfuxapvnC+JHubty7cV1mTyh6ZimKa2uNgMg3gCSE0UCVTCUYAzBV27nX+w8GL18CHx+TMj8RxAp9KFu9BkWVTyvlZQa7AeABBHRaTYGGXJs3B6Al1swYxLSTfI1cv46hzku59jDt+6X1a1C8dKltDbcATEI4ZDUFmqfbvBEAk6NmR2rrESc/dvGCbeNSEwPrXrBdCe4C4BHkaEVmALRGjzGwNxOmiZ4/0HZGue1MRUu0o4ptr2KW368M1E0A8SoAP241Bt+a8tqUryhLQqgl2p96+mOXL0nfcGXXFzfmwOo1suHJONcBcB6xmoKzHQMQao3uZGBfpp7+yHffKhslkyCqQHVEdRvAZBXsshqDp7Jdo3YLCmW0n+E/ezDc9auMn8oxJXXPo2R5tXTetptf4bOzYSSf9OFAeOvXaF+4SVrDRCDnOGI1BfY7BCD2IwNeSogPtP+Msdu9Jvb9iIZvUSXK178opR03/1w4a2z45fxC4MBPVmNgszMAWmIdqb9y6G/7IfkJV8ophSDxiXn2lq1SGSe+3471fecxdA/ovwtMcGBuMVBaBLTP34jwK99I6ZgImu4zgXYLqmqNpT36d+f0Fyb2PKXGvB27pfQTvf/vCDA+uUN/AbCkHIj6KlC/54aUjqmga42BrF7PSAAxXzlW7blpylspnRkHwEstSBDyBICy4lnYu2sZGmqCqJzzlNTJyQzafWahVJ6XbsKeACDMP7K3DjVLyqQMnCpIFoDIFxDe7m7Bc5ErCIwNxG++h1d+mPcx1BMA3tuzCK9tWq5lvkhWAaC9mEEB11tQ0/YxVATLsLGhDqXF9toPAchyImTHUAFAvHy+wjiEBXPsfT9PFZABQRVAIr2uOoQV1SHlIicANgEcf385Ll7pTstevHAe1tXVwu8rlAZBAGwCOHtkA/qjMZzvuIrhkbtJFXE/2NCwArOD6Q9QmZiCpKnmIdD1m7AAIF6jY+M439GFvv8ePsck7gsNtdWoWrwopxVUARoVkJra1WPhao+VpiYArFtZOy0EAmAIgJC5+e8dtF/5DePj95OquUZVAmAQgJAaGrmLc5e7MBAbTCpPN6oSAMMAEveFju4eWLdup6lnG1UJgAMAEpLXbt3OOaoSAAcBCOlcoyoBcBhArlH13d9X5RxVvRjgmc8BKuZkG1VP+l9XkfBM7GMJINuoSgDy0IIyj23qqEoAXACQOqp+3Jd8BMkz7UVmI49tC8q8OJqCXKqAxLIEwCaAkx+tReXcIplqnTaGANgE8MEbNdi+fgEByHAgb0/GxR9L2bcSNYv1/gKSKsBmBYg0AWHfrmWory633Y5mDIDQ0dhmcP45A1P65jzxpK52aWk3K28JKI+hVS1RC4zZ+vNEAYEApB8AdQCt0QjAym2dIyLwiG3KAEQLYhP8mHIVcIAzqoBMAsoAbJ18F5P4iWel/kc0C//hqe7oqc3o8CMAOu4ZyCUABkzUkSAAOu4ZyOW/NMvdA9Y2e6rtemozOhwIgI57BnIJgAETdSQIgI57BnIJgAETdSQIgI57BnIJgAETdSQIgI57BnIJgAETdSQIgI57BnIJgAETdSQIgI57BnIJgAETdSQIgI57BnIJgAETdSQIgI57Mzj3ifk+4HFlSABcJkcACIDLDri8PFUAAXDZAZeXpwpwGcD/adBAjgnH7eIAAAAASUVORK5CYII="
                             width="65" height="65">
                        <h3 class="mb-4"><strong>{{ trans('site/contact.How can we help you?') }}</strong></h3>

                        @if (session()->has('message'))
                            <div class="alert alert-{{ session()->get('message.type') }}">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! session()->get('message.content')  !!}
                            </div>
                        @endif

                        @if (Session::has('flash_message'))
                            <div class="alert alert-{{ Session::get('flash_message.level') }}">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                @if (is_array(Session::get('flash_message.message')))
                                    @foreach(Session::get('flash_message.message') as $error)
                                        {!! $error !!}
                                    @endforeach
                                @else
                                    {!!  Session::get('flash_message.message')  !!}
                                @endif
                            </div>
                        @endif

                        @if(isset($errors))
                            @foreach ($errors as $message) {
                            {{ $message }}
                            @endforeach
                        @endif

                        {!! Form::open() !!}

                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'sr-only']) !!}
                            {!! Form::text('name', null, ['class'=>'form-control input-lg', 'placeholder'=>trans('site/contact.Your Name'), 'required']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('email', 'Email', ['class' => 'sr-only']) !!}
                            {!! Form::email('email', null, ['class'=>'form-control input-lg', 'placeholder'=>trans('site/contact.your@email.com'), 'required']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('message', 'Message', ['class' => 'sr-only']) !!}
                            {!! Form::textarea('message', null, ['class'=>'form-control input-lg', 'placeholder'=>trans('site/contact.Message'), 'required', 'rows'=>3] ) !!}
                        </div>

                        <button class="btn btn-primary btn-lg">
                            {{ trans('site/contact.Send') }}
                        </button>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection