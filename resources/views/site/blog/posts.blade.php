@extends('site.layouts.main')

@section('meta_title'){{trans('metas.blog_title')}}@endsection
@section('meta_description'){{trans('metas.blog_description')}}@endsection

@section('meta_languages')
    <link rel="alternate" hreflang="en" href="https://www.aprus.com/blog"/>
    <link rel="alternate" hreflang="es" href="https://es.aprus.com/blog"/>
@endsection

@section('content')

    <div class="container mb-5">
        <div class="row">
            <div class="col-md-4">
                <a class="card card-post mb-4" href="{{ route('site.home') }}">
                    <img class="card-img-top"
                         src="{{ asset('https://res.cloudinary.com/aprus/image/upload/v1485964512/blog/tablet-with-aprus.jpg') }}">
                    <div class="card-body">
                        <p class="card-title">{{ trans('site/post.Learn Russian online having fun!') }}</p>
                        <p class="mb-0">{{ trans('site/post.learn_russian_post_excerpt') }}</p>
                    </div>
                </a>
            </div>

            @foreach($posts as $post)

                <div class="col-md-4">
                    <a class="card card-post mb-4" href="{{ route('site.blog.post', ['title_slug' => $post->title_slug]) }}">
                        <img class="card-img-top" src="{{$post->featured_image}}">
                        <div class="card-body">
                            <p class="card-title">{{$post->title}}</p>
                            <p class="mb-0">{{$post->excerpt}}</p>
                        </div>
                    </a>
                </div>

            @endforeach
        </div>
    </div>

@endsection

