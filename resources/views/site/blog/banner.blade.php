<?php
$language = \App::getLocale();
?>

<div id="learn_russian_having_fun_image" class="text-center pt-4">
    <div class="box-post-learn-russian text-center">
        <img src="https://res.cloudinary.com/aprus/image/upload/v1502029184/shared/lena-learn-russian-face.jpg">
        <p class="lead text-primary">{{ trans('site/post.banner_title') }}</p>
        <p class="box-post-learn-russian-text text-muted">{{ trans('site/post.banner_text') }}</p>
        <div class="text-center"><a class="btn btn-lg btn-primary" href="{{ route('register') }}">{{ trans('site/home.try_free') }}</a></div>
    </div>
</div>
