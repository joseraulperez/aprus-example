@extends('site.layouts.main')

@section('body_class'){{'post'}}@endsection
@section('meta_title'){!! $post->meta_title  !!}@endsection
@section('meta_description'){!! $post->meta_description !!}@endsection
@section('meta_og')
    <meta property="og:type" content="article">
    <meta property="og:title" content="{{ $post->title }}">
    <meta property="og:description" content="{{ $post->excerpt }}">
    <meta property="og:image" content="{{ $post->featured_image }}">
@endsection
@section('content')

    <div class="container mt-4 mb-5">

        <div class="row d-flex justify-content-between">
            <div class="post col-lg-8">
                <h1 class="title mb-4">{{ $post->title }}</h1>
                <div class="lead mb-4">{{ $post->excerpt }}</div>

                <div class="post-featured-image mb-5">
                    <img src="{{ $post->featured_image }}" title="{{ $post->title }}">
                </div>

                <div class="row d-flex justify-content-start">
                    <div class="col-lg-2">
                        <div class="post-author">
                            <img class="post-avatar" src="{{ asset('https://res.cloudinary.com/aprus/image/upload/v1501860063/blog/avatar-post-lena.png') }}">
                            <p class="mb-0 text-muted post-author-name">Elena Markina</p>
                        </div>
                        <div class="post-share">
                            <p class="lead text-muted">Share</p>
                            <div class="share-box" id="share">
                                <!-- facebook -->
                                <a class="facebook" href="https://www.facebook.com/share.php?u={{ url()->current() }}&title={{ $post->title }}" target="blank"><i class="fab fa-facebook-f"></i></a>

                                <!-- twitter -->
                                <a class="twitter" href="https://twitter.com/intent/tweet?status={{ $post->title }}+{{ url()->current() }}" target="blank"><i class="fab fa-twitter"></i></a>

                                <!-- google plus -->
                                <a class="googleplus" href="https://plus.google.com/share?url={{ url()->current() }}" target="blank"><i class="fa fab fa-google"></i></a>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="content">
                            {!! $post->content !!}

                            @if($post->has_try_for_free_button)
                                <div class="text-center mt-5 mb-5">
                                    <a class="btn btn-lg btn-primary" href="{{ route('register') }}">{{ trans('site/home.try_free') }}</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                @include('site.blog.banner')
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        window.onscroll = function() {myFunction(); sideBoxSticky();};

        // Get the navbar
        var share = $(".post-share");

        // Get the offset position of the navbar
        var sticky = share.offset().top - 25;

        // Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
        function myFunction() {
            if (window.pageYOffset >= sticky) {
                share[0].classList.add("sticky")
            } else {
                share[0].classList.remove("sticky");
            }
        }

        var side_box = $("#learn_russian_having_fun_image");

        // Get the offset position of the navbar
        var side_box_sticky = side_box.offset().top - 25;

        // Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
        function sideBoxSticky() {
            if (window.pageYOffset >= side_box_sticky) {
                side_box[0].classList.add("sticky")
            } else {
                side_box[0].classList.remove("sticky");
            }
        }


        $(document).ready(function(){

            var blog_audio = {

                text_audio : null,
                comment_alert_success : null,
                comment_alert_error : null,
                comment_alerts : null,
                btn_reply : null,
                body : null,

                setObjects : function() {
                    this.play_audio = $(".play-audio");
                },

                setEvents : function() {
                    this.play_audio.click(this, this.playAudio);
                },

                playAudio : function(event) {
                    var url = $(event.currentTarget).attr('data-audio');
                    var audio = new Audio(url);
                    audio.play();
                    event.preventDefault();
                },

                init : function() {
                    this.setObjects();
                    this.setEvents();
                }

            };

            blog_audio.init();
        });
    </script>
@endsection