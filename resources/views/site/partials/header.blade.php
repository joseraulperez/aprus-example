<nav class="navbar navbar-header navbar-expand-lg mb-5 box-shadow ">
    <div class="container">
        <a class="navbar-brand" href="{{ route('site.home') }}">
            @include('site.partials.logo')
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link mr-lg-3" href="{{ route('login') }}">{{ trans('site/header.login') }}</a>
                <li class="nav-item">
                    <a class="btn btn-outline-light" href="{{ route('register') }}">{{ trans('site/header.register') }}</a>
                </li>
            </ul>
        </div>
    </div>
</nav>