<div class="footer pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <p class="copyright">Copyright Aprus © {{date('Y')}}</p>
            </div>
            <div class="col-md-2">
                <ul class="list-unstyled">
                    <li class="mb-2">{{ trans('site/footer.Pages') }}</li>
                    <li><a href="{{ route('site.home') }}">{{trans('site/footer.Home')}}</a></li>
                    <li><a href="{{ route('login') }}">{{trans('site/footer.Log In')}}</a></li>
                    <li><a href="{{ route('register') }}">{{trans('site/footer.Sign Up')}}</a></li>
                    <li><a href="{{ route('site.blog') }}">{{trans('site/footer.Blog')}}</a></li>
                </ul>
            </div>
            <div class="col-md-2">
                <ul class="list-unstyled">
                    <li class="mb-2">{{ trans('site/footer.Company') }}</li>
                    <li><a href="{{ route('site.about_us') }}">{{trans('site/footer.About Us')}}</a></li>
                    <li><a href="{{ route('site.contact') }}">{{trans('site/footer.Contact Us')}}</a></li>
                    <li><a rel="nofollow" href=" {{ route('site.privacy_policy') }}">{{trans('site/footer.Privacy Policy')}}</a></li>
                    <li><a rel="nofollow" href="{{ route('site.terms_of_service') }}">{{trans('site/footer.Terms of Use')}}</a></li>
                </ul>
            </div>
            <div class="col-md-2">
                <ul class="list-unstyled">
                    <li class="mb-2">{{ trans('site/footer.other_languages') }}</li>
                    <li><a href="https://www.aprus.com">English</a></li>
                    <li><a href="https://es.aprus.com">Español</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
