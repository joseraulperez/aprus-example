@extends('auth.layouts.main')

@section('meta_title'){{'Log in to Aprus'}}@endsection
@section('meta_description'){{trans('metas.home_description')}}@endsection

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="card mb-2">

                    <div class="card-body">
                        <div class="progress mb-4">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                                 aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"
                                 style="width: 75%">{{ trans('auth/register.75% complete') }}</div>
                        </div>

                        <h2 class="text-center"><strong>{{ trans('auth/register.You are almost there!') }}</strong></h2>

                        <div class="text-center">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAf5SURBVHhe7ZzbbxtFFMYjroI34BH+AgRPrU1BSBU2al+4iEIESDzzAuIBHlrKpVxKS4UKpZe0hZA0IaRJSinFaZpCSElIc6+de0xiCUGpBBRQWhMJHtAw3zKmrv2Ns2vvJJt4PuknWfaZOed8u17vjC1XWFlZWVlZWVlZWVlZWVlZWVlZWVlZMY1Op+5MJGd3JaZTk/Fk6s9EMiVWMuhRMoGeR6ZSdygbFl8zMzPXS9P3xadn/2GFlgPoPT6d2jsxMXGdsmVx1Nzbe0N7z/AgK6ocae8ZGoQnyh7z2t8SO/vi+x+JhtYOnAW0qHIAvcMDeLG/OTak7DGrppOd65Fw065qh531R8SZkQla4EoGPaP3jA/w5HDb1+uUTeZU1Xx8MJM0w5Z9dSLW1UcLXYmgV/Sc60NV0xf9yiZzau0e+P61qvq85KD6aJsYnvqOFr0SQG/okfUOT2Jd/d8rm8xJ3oJd6hudErsaPqOFbKtuFKeHRmkDyxn0hN5Yz/ACnsAbZZM5ZQqSyURT+zfipd01eQVtls+1nOpyYrKbWI6gB/SCnnL7RO/wILtPZZM5ZRcHOgdHxLYP+Zmxu/GY6B+bviJ+OYHa0QPrDT2j99wxyiZzyk0Ihia/Ex98eoIW+vqBetF+ZihvTNBBzaid9YRe0TMbp2wyJ5Y0w/HTveKVfYfyCsYtWv0XXy6LNQNqRK3Zt9oZ0Bt6ZOMyKJvMiSXNpicxId451JJXPHintsV5nY0LAk7tskZau+zJTe3KJnNiSXNxzqLYV/Qsenlv7YJn0VKAmlBbbr3Ou1f24vbdq2wyJ5ZUx6neYe119OCRVu11dDFBDaiF1Yja0QMbp0PZZE4saSEGxpNiT+PntMG3nDuJRN6YxQK5UQOrDTWjdjauEMomc2JJ3XDky+4C99KnF3XN8N8a5rR2DYNa2Tg3KJvMiSV1yzfDo2J79eG8poGzmhybouP8BDl0q3jUhhrZOLcom8yJJfXC8OSM+LDAfkpbzyAd5weYW7ePhZpQGxvnBWWTObGkxRDr6hevsjWDpPbzU+LslH9rBsyFOTF3bj7UgFrYuGJQNpkTS1osZxKTYmfd5T31bHbUNIvu+Dgd5wXMgblYDuRGDWxcsSibzIklLQXcX39y4r9vlXINenlPrTjW2UPHuQFjMUfuvMiFnCZW5somc2JJ/eCrvrPijQMNeWaBquaYGJpwv2ZALMawuZADudg4P1A2mRNL6heD8r67quk4Ne7Ngw2io3/hNQNiEMvmwNzIwcb5hbLJnFhSv/mso0feo/NLR2NbJ7104Dm8xi5lmAtz5o4xgbLJnFhSE3SfHRVv1zTlmQne+/io6B29vGbAYzzHYjEH5sqe2yTKJnNiSU0xPDUjao6109vHLVV1orV7wAGPc1/HGIzFHGxuUyibzIklNU3bt3qTdQcHY9hcplE2mRNLuhhgC+G9Bn6ZyQYxXrc0RuS7JNneLn54f6f45flnxR9PPSIuPXifSK+72wGP/3hqg/MaYhCbmObvLGWTObGkiwU+aA+f7BSbyQctnsNrXu7tx/sGxLkdW8XchnXiz2jYE3OPrnfGjvdduXWibDKn7GRLxdcDcbH1g0/+Nx+P8RyLZYwNJ8T5VzfJs/seaq4XMMdPr2125sTcyiZzym1mqcBia39LzMHLIi3VUCcvKRFqZilcfDjqzK1sMifW1HJgZDIpz/qN1Dw/SUdDB8SqVdcqu/wXay7ojI5Pil+ee5oaZgJ5EE6IB1bdqCzzV6zBIIMz36v5OrFYHfIgHDfyTmBNBpliLjs6sdhC4HKkbPNPrMmgkvr4EDVmIXRisQuRjq5+XFnnj1ijQWRsOC4uPlTc3Y5OLNYFFy5GQ7co+0oXazaInN/yIjPDFTqxWHeE9ir7ShdrNmhghVvKIksnFuuGdCT893xkza3KwtLEGg4a2CJgRrhFJxbrFnkQtisLSxNrOFBMzzj7NMwEt+jEYt0iD8CPYkvFVcrG4hVPpi7RxgNC8uRJaoAXdGKxXpiPrAorG4uXPAATrPGggO1i1rwXdGKxnrg/tEnZWLyc/4UgjQeFn194hjfvAZ1YrDdCR5SNxQt/UBEP8P9D4IsT3rx7dGKxnoiEx5WNpSkxPVvFmg8C+PaKNu8BnVisJyLh35SFpQn/DiIPQgczYKnx40sWnVisF7AeUBaWLhwE/FVL0C5HZXMAMorPzNwuD8S78eTseBBuUcviEhRk4YOONu8BnVisJyKhMVXmypU8AEdp8x7QicV6w4fb0KBLHoDNvPkAEAltVGWuXM3fH15Dmw8A89HVIVXmyhU2vNLR0DlmwFLi22bcchC2fpkJS0k6Etqmylv5ml8bvg333MwIN+jEYt0ga/nLty9klovS0fB+ZoYbdGKx7gjtUWWVj/BFOBY+3JDC6MRiFyQS/nVu3ZqbVVnlpfR94SepKQugE4tdCHn5qVTllKfkh99BZkwhdGKxBYmEd6syylei8vbr8DNBapAGnVisDueniZWVV6syylv4oax8J7Qyo0wgLzsxYz/OXa7CD2XxW01mmL+E9oi1a69Raa1ylY7c9YQ06kK+cSVz4VIk/JhKY1VIzi1qNLS3lMVaBiyycNbP3XvvTWp6K7dSK+btkh+YuYXAGGwvlN0K14SwSYYfTeF3O9izxxcnkt/xDgF4LJ8fVa9txK5m2WysWVlZWVlZWVlZWVlZWVlZWVlZWVkZUEXFv7bihFRNgjveAAAAAElFTkSuQmCC">
                        </div>

                        <p class="lead">{!! trans('auth/register.verify_your_email') !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection