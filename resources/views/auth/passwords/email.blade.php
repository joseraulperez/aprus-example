@extends('auth.layouts.main')

@section('meta_title'){{ trans('metas.recovery.title') }}@endsection
@section('meta_description'){{ trans('metas.recovery.description') }}@endsection

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="card">
                    <div class="card-body">
                        <h3 class="text-center mb-4">{{ trans('auth.recovery.title') }}</h3>
                        <p>{{ trans('auth.recovery.text') }}</p>

                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        {!! Form::open(['method' => 'post', 'route' => 'password.email']) !!}

                        <div class="form-group">
                            {!! Form::label('email', 'email', ['class' => 'sr-only']) !!}
                            {!! Form::text('email', old('email'), ['placeholder' => trans('auth/register.placeholder_your_email'), 'required' => 'required', 'class' => 'form-control'. ($errors->has('email') ? ' is-invalid' : '')] ) !!}
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="text-center">
                            {!! Form::submit(trans('auth.recovery.btn_send_reset'), ['class' => 'btn btn-block btn-primary btn-lg']) !!}
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
