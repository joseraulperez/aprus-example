@extends('auth.layouts.main')

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="card mb-3">

                    <div class="card-body">
                        <h3 class="text-center mb-4">{{ trans('auth/reset.title') }}</h3>
                        <p>{{ trans('auth/reset.text') }}</p>

                        @include('auth/partials/messages')

                        {!! Form::open(['method' => 'post', 'route' => 'password.request']) !!}

                        {!! Form::hidden('token', $token) !!}

                        <div class="form-group">
                            {!! Form::label('email', 'email', ['class' => 'sr-only']) !!}
                            {!! Form::text('email', old('email'), ['placeholder' => trans('auth/register.placeholder_your_email'), 'required' => 'required', 'class' => 'form-control'. ($errors->has('email') ? ' is-invalid' : '')] ) !!}
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group">
                            {!! Form::label('password', 'password', ['class' => 'sr-only']) !!}
                            {!! Form::password('password', ['placeholder' => trans('auth/register.placeholder_password'), 'required' => 'required', 'class' => 'form-control'. ($errors->has('password') ? ' is-invalid' : '')] ) !!}
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            {!! Form::label('password_confirmation', 'password_confirmation', ['class' => 'sr-only']) !!}
                            {!! Form::password('password_confirmation', ['placeholder' => trans('auth/reset.placeholder_password_confirm'), 'required' => 'required', 'class' => 'form-control'. ($errors->has('password') ? ' is-invalid' : '')] ) !!}
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="text-center">
                            {!! Form::submit(trans('auth/reset.btn_reset'), ['class' => 'btn btn-block btn-primary btn-lg']) !!}
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
