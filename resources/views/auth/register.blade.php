@extends('auth.layouts.main')

@section('meta_title'){{ trans('metas.register.title') }}@endsection
@section('meta_description'){{ trans('metas.register.description') }}@endsection

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="card mb-2">

                    <div class="card-body">
                        <h3 class="text-center mb-4">{{ trans('auth/register.title') }}</h3>
                        <p class="text-center">{{ trans('auth/register.description') }}</p>
                        <p class="text-center text-muted">{{ trans('auth/register.use_social_network') }}</p>

                        <div class="text-center">
                            {{--                            <a class="btn btn-facebook mb-3" href="{{ route('login.provider', ['facebook']) }}">
                                                            <i class="fab fa-facebook-f mr-2"></i> Facebook
                                                        </a>
                        --}} <a class="btn btn-twitter mb-3" href="{{ route('login.provider', ['twitter']) }}">
                                <i class="fab fa-twitter mr-2"></i> Twitter
                            </a>
                            <a class="btn btn-google mb-3" href="{{ route('login.provider', ['google']) }}">
                                <i class="fab fa-google mr-2"></i> Google
                            </a>
                        </div>

                        <p class="text-center text-muted">{{ trans('auth/register.use_email') }}</p>

                        {!! Form::open(['method' => 'post']) !!}

                        <div class="form-group">
                            {!! Form::label('name', 'name', ['class' => 'sr-only']) !!}
                            {!! Form::text('name', old('name'), ['placeholder' => trans('auth/register.placeholder_your_name'), 'required' => 'required', 'class' => 'form-control'. ($errors->has('name') ? ' is-invalid' : '')] ) !!}
                            @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group">
                            {!! Form::label('email', 'email', ['class' => 'sr-only']) !!}
                            {!! Form::text('email', old('email'), ['placeholder' => trans('auth/register.placeholder_your_email'), 'required' => 'required', 'class' => 'form-control'. ($errors->has('email') ? ' is-invalid' : '')] ) !!}
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group">
                            {!! Form::label('password', 'password', ['class' => 'sr-only']) !!}
                            {!! Form::password('password', ['placeholder' => trans('auth/register.placeholder_password'), 'required' => 'required', 'class' => 'form-control'. ($errors->has('password') ? ' is-invalid' : '')] ) !!}
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group">
                            {!! Form::submit(trans('auth/register.btn_join'), ['class' => 'btn btn-primary btn-block btn-lg']) !!}
                        </div>

                        <p class="text-muted text-left mb-0">
                            <small>{{ trans('auth/register.terms') }}</small>
                        </p>
                        {!! Form::close() !!}

                    </div>
                </div>

                <p class="text-muted text-center">{!! trans('auth/register.login') !!}</p>

            </div>
        </div>
    </div>
@endsection
