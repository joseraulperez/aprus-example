@extends('auth.layouts.main')

@section('meta_title'){{'Log in to Aprus'}}@endsection
@section('meta_description'){{trans('metas.home_description')}}@endsection

@section('content')

    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <div class="panel ">
                <div class="panel-body text-center">
                    <div>
                        <img class="icon icons8-Checked payment-success-icon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAHYUlEQVR4Xu2dT28bRRjGn1nATnIhlQolzqGtegOVNhKN16emEvRUaLiCgPYT1P0EbT9B0k/QAIIrDuJUkJqebAekpCC4oaaHOG2FVOeSxAb8otn1htZyvTM7Mzuz1q5Utapn58/z2/edeWdmZxnyy6oCzGrpeeHIAVh+CHIAOQDLClguPreAHIBlBSwXn1tADsCyApaLzy0gBzBagfWd9fMEOgGA/wERzgI0DWLTYPzf/D+xCUZtgLUZw2Y/xy0GtjU/M3/fssYji3fOAjaebUx3D7qXqUcLAFsEw7SSgIQ2QDXmsbXCRGF17shcWyk/zTc7AaC+Uz/BiF0G2AKARc1tHMyuBtAaMVqtzFS2DJcVm71VAIHwPXYDjF2JramJBEQr5NEtmyCsAOi7mWvUQ1XZxaiCIbSZh+XCROG2DfeUKgCnhB8EZwlEagCareYigZYAFoxm3L2Ij56ul0vlWhp1TAVAo9W4BrDlNBqkrwyq+iX/tr78hudkFAB3OZ29zpK1TlZVPaKV4lTxusm+wRiAUPzuvcNgSVUMW/cTNotThQumIBgBsP50/Wzvb+LiqwVRtkQf0kF7r7EL82/OR1G2tpppBzB24kdSE9omIGgF0Hc7D8fmyR9iCcWpwkmd7kgbgLHx+XHOhbDpz5bn4pKJ/q4NQGO7cSezox1RtQ7dEa34s/5V2duGpdcCoNFqVAG2pKNC2cmDrvslXzm2UQYQRrj4LjvC6aspAz5WjZiVAAR+f7+z4f70gj7RX8yJtoqTxTmVTlkJQH27eZMx3DDVvCzkS4RbldnyzaR1TQxgXIecT/ae4PHeU5w5elpMU0JbZWiaGMA4Pv1c/K//+BYH/x7gw1OXhCGoWEEiAOFKlrcxTgHX8+JHj74wBEKbvN5ckpW1ZABazRUGfCFmo+6nGiZ+VOv5t87h4vH3YxtBwJeVUll6aVUaQLiA7j2MrVFGEowSP2rCu0dP46NTl2JbRKx3UtYKpAGMU9AlIv7/lvAeLh7/IAaCfHCWBEAN4FtIsn3JiF/0ivj8nU9xbOpYHIBVv+RLbauRAhAGXt1n2ZYeMCN+qEpxsnBEJjCTAlDfWb/CiO5kGYBJ8bkustMTcgAyPvoxLT4HIDsakgLQ2G4+y+rYPw3xA89AaPuz5SOiXkIYQH3n5wVGvXuiGbuULjXx+40m5l2ozJxbE9FAAkA2/X/a4gdGwNjVysz8il4AGZz5tCF+AEBihlTcAjIGwJb45gC0mmsMOC9iVrbT2BQ/bDsJB2TiFqAZwN1HP+HMG6cFoks5nPbFD4ai9yulMn/ZJPYSBtDYbvLp5/CdLMXr+z9/wK9//YaJVybw2dufaIPggvj9oajw1hVxAK0mKeoe3B6JH+WlC4Iz4vcb5pfKQtoKJeJ5NjQAGBRfFwTXxOft0g9gu7kJhjNJreBl4qtCcFF8EB74s2Uhdy1sAXWFTvjuox+x/viXWHay7shJ8cP5IAOdcKuReB2AC/XV79+g0+tog+Cq+OaGoYqBmE4IbovvcCSsA4Lr4puLhDUtxqhAyIL4HIDMoox4J6xxOjoJBN6waNNUXEcivoYbl1Oy341MR/djAX7QxevJqvXiXbIQ+N18x1rcZVt8ALt+qSz8bpywBfCG1zUvScpAiBOe/+6A+GaXJE0syuuC4IL4sv6/n17k2QrTmNqWogrBFfEDKzS5LSXsB5IHZKNQJ4Xgkvgy6wCRFlJ9QB+AsffBZCG4JX4QAUi/NyYNwPTmXFEI7onPF+NT2JxrYjQ06JriIDgpflrb0wMA4RZ1fm6ClphgWN/wMgguis/H/sXJwgmZPaGJ+4DoxjReURqE4Kj4UttQBh826T4gyqA/JOWnDhqzAl5WBIH/W2yLuPiwWlPKxE+/dBwwWOE0rCCCwP+O35+vSVKJbGQ2YQ3LNrEFRIHZwX5nk4Edl6jz2CQl0KNKyVc6A08JAFcyP6pA7XA/ZQCmgzN3zUU+6NLugp7PUPdMqbvCy7+EMaotWiwg6g86e901la0rLot+WDeJLSci7dEG4BDCftf40FSkYYbSKA05jbqgKPPg0L5/iL8dYjQ+MCTwqGx3vVfZgu6TE7VawBhDMCK+ciA26nHpH2eT/T6B8KA4VVhIMs8jYqVGLCAqmEM42O8uZ/VgD9lXTkUEH0xjFEBUWDbPl9Azzo+DkgqAKGLugZZdn7bg0wseWFX1ML444aPfUwMQDVMP9rpVxlB1cJS0S4RllfPfREV/Pl2qAF7oG9wBEQg/MVVYNtXRphIJJ6HPV9ZA3k1bnTTvZCcmC1UbwltxQS+D1P+MFT9nh387zPBZRLQKgH/GqiZ7ulWShyzuHisuKC5+6O53F3rAIgu/KaYaUe8SUPOAWmGysGbzaU9lKiKOuOzv/JAQ8E8Z9oLPGYIxOktg04zAP2UYvrNGeEAMbQZqE7HwIwse2wLYluihGbL10pXeOQvQ1bCs5JMDsEwqB5ADsKyA5eJzC8gBWFbAcvG5BeQALCtgufjcAnIAlhWwXPx/PccanTvvPQ4AAAAASUVORK5CYII=" width="72" height="72">
                    </div>

                    <br>

                    <p class="lead text-center"><strong>Nice to meet you {{ $name }}</strong></p>

                    <p>Verify you email address to access to Aprus. We have sent you a confirmation email to <strong>{{ $email }}</strong>.</p>
                    <p>Your adventure of learning Russian starts today!</p>
                </div>
            </div>
        </div>
    </div>

@endsection