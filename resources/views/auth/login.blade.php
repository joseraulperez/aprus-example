@extends('auth.layouts.main')

@section('meta_title'){{ trans('metas.login.title') }}@endsection
@section('meta_description'){{ trans('metas.login.description') }}@endsection

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="card mb-3">

                    <div class="card-body">
                        <h3 class="text-center mb-4">{{ trans('auth/login.title') }}</h3>
                        <p class="text-center text-muted">{{ trans('auth/login.use_social_network') }}</p>

                        <div class="text-center">
{{--                             <a class="btn btn-facebook mb-3" href="{{ route('login.provider', ['facebook']) }}">
                                <i class="fab fa-facebook-f mr-2"></i> Facebook
                            </a>
--}}                            <a class="btn btn-twitter mb-3" href="{{ route('login.provider', ['twitter']) }}">
                                <i class="fab fa-twitter mr-2"></i> Twitter
                            </a>
                            <a class="btn btn-google mb-3" href="{{ route('login.provider', ['google']) }}">
                                <i class="fab fa-google mr-2"></i> Google
                            </a>
                        </div>

                        <p class="text-center text-muted">{{ trans('auth/login.use_email') }}</p>

                        @include('auth.partials.messages')

                        {!! Form::open(['method' => 'post']) !!}

                        <div class="form-group">
                            {!! Form::label('email', 'email', ['class' => 'sr-only']) !!}
                            {!! Form::text('email', old('email'), ['placeholder' => trans('auth/register.placeholder_your_email'), 'required' => 'required', 'class' => 'form-control'. ($errors->has('email') ? ' is-invalid' : '')] ) !!}
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            {!! Form::label('password', 'password', ['class' => 'sr-only']) !!}
                            {!! Form::password('password', ['placeholder' => trans('auth/register.placeholder_password'), 'required' => 'required', 'class' => 'form-control'. ($errors->has('password') ? ' is-invalid' : '')] ) !!}
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif

                        </div>

                        {{--
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    Remember Me
                                </label>
                            </div>
                        </div>
                        --}}

                        <div class="form-group text-center">
                            {!! Form::submit(trans('auth/login.btn_login'), ['class' => 'btn btn-block btn-primary btn-lg']) !!}
                        </div>

                        {!! Form::close() !!}


                        <div class="text-center">
                            <a href="{{ route('password.request') }}">
                                {{ trans('auth/login.forgot_password') }}
                            </a>
                        </div>
                    </div>
                </div>

                <p class="text-muted text-center">{!! trans('auth/login.sign_up') !!}</p>

            </div>
        </div>
    </div>

@endsection
