<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Email Verification
    |--------------------------------------------------------------------------
    */
    'email_verified' => 'Email verificado correctamente, ya puedes acceder a tu cuenta.',
    'error_verify_email' => 'Tienes que verificar tu email. Te hemos enviado un correo electrónico pra activar tu cuenta, por favor, revísalo.',

    /*
    |--------------------------------------------------------------------------
    | Login
    |--------------------------------------------------------------------------
    */
    'title' => '¡Bienvenid@ otra vez!',
    'use_social_network' => 'Continua con tu red social favorita',
    'use_email' => 'O entra con tu email',
    'sign_up' => '¿Aún no tienes una cuenta? <a href="' . route('register') .'">Regístrate</a>',
    'forgot_password' => '¿Olvidaste la contraseña?',
    'btn_login' => 'Entrar',
];