<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Register Success
    |--------------------------------------------------------------------------
    */
    '75% complete' => '75% completado',
    'You are almost there!' => 'Ya casi has acabado!',
    'verify_your_email' => 'Tenemos que verificar que eres una persona real. Para ello, te hemos enviado un email. ¡Haz click en el botón que encontrarás en ese email para comenzar tu aventura!.<br><br>Si no lo recibes, comprueba que no haya ido a parar a tu carpeta de correo electrónico no deseado.',

    /*
    |--------------------------------------------------------------------------
    | Register
    |--------------------------------------------------------------------------
    */
    'title' => 'Aprende Ruso Divirtiéndote',
    'description' => 'Únete a la comunidad de Aprus y consigue acceso gratuito al curso de introducción al Ruso... ¡y mucho más!',
    'use_social_network' => 'Puedes unirte usando tu red social favorita',
    'use_email' => 'O usando tu mejor email',
    'terms' => 'Al registrarte, aceptas las Condiciones de Servicio y la Política de Privacidad de Aprus.',

    'placeholder_your_name' => 'Tu Nombre',
    'placeholder_your_email' => 'tu@email.com',
    'placeholder_password' => 'contraseña',
    'btn_join' => '¡Quiero Unirme!',
    'login' => '¿Ya tienes una cuenta? <a href="' . route('login') .'">Inicia Sesión</a>',

];