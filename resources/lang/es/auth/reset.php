<?php

return [
    'title' => 'Cambiar contraseña',
    'text' => 'Escribe tu email y la nueva contraseña para tu cuenta.',

    'placeholder_password_confirm' => 'repite la contraseña',
    'btn_reset' => 'Cambiar Contraseña',
];