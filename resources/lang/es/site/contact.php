<?php
return [
    'your@email.com' => 'tu@email.com',
    'Your Name' => 'Tu Nombre',
    'Message' => 'Mensaje',
    'Get in Touch' => 'Ponte en Contacto',
    'How can we help you?' => '¿Cómo podemos ayudarte?',
    'Send' => 'Enviar',
    'Message sent successfully' => 'Mensaje enviado correctamente',
];