<?php

return [
    'author' => 'por :first_name :last_name',
    'Continue reading' => 'Sigue leyendo',
    'Try our Course Online Free!' => 'Prueba nuestro curso online gratis',
    'Learn Russian online having fun!' => 'Aprende Ruso online divirtiéndote!',
    'learn_russian_post_excerpt' => '¿Te gustaría aprender Ruso? ¡Te explicamos cómo!',

    // Banner
    'banner_title' => 'Aprende Ruso divirtiéndote',
    'banner_text' => 'Prueba nuestro curso de Introducción al Ruso Gratis',
];