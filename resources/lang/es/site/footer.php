<?php

return [
    'Home' => 'Home',
    'Blog' => 'Blog',
    'Contact Us' => 'Contáctanos',
    'Terms of Use' => 'Términos de Uso',
    'Privacy Policy' => 'Privacidad',
    'About Us' => 'Sobre Nosotros',
    'Aprus in other languages:' => 'Aprus en otros idiomas:',
    'Log In' => 'Iniciar Sesión',
    'Sign Up' => 'Regístrate',
    'Pages' => 'Páginas',
    'Company' => 'Compañía',

    'other_languages' => 'Aprus en'
];