<?php
return [
    'title' => 'Sobre Nosotros',
    'subtitle' => 'Afincado en Barcelona (España), APRUS nació porque nos encanta enseñar Ruso. Hacemos clases presenciales, cursos online, libros... cualquier cosa que te ayude a conocer Rusia y su cultura',

    'Founder' => 'Fundador',
    'Founder-girl' => 'Fundadora',
    'contact_us_title' => '¿Alguna pregunta?',
    'who_is_behind_title' => '¿Quién hay detrás de APRUS?',
    'lena_bio' => 'Elena es Doctora de Filología Rusa y especialista en Ruso como lengua extranjera. Lleva más de 10 años enseñando Ruso a alumnos de todo el mundo. Le gusta leer, viajar y bailar.',
    'raul_bio' => 'Ingeniero y emprendedor. Raul es un entusiasta de las nuevas tecnologías. Ha trabajado en empresas con enormes volúmenes de usuarios.',

    'btn_contact' => 'Contacta',
];