<?php
return [
    'title' => 'Aprende Ruso Divirtiéndote',
    'subtitle' => 'Prueba gratis nuestro curso online y aprende Ruso con cientos de imágenes, audios y videos',

    'designed_by_teachers_text' => 'Años de experiencia con alumnos de todo el mundo nos dan una visión única de cuál es la mejor manera de aprender Ruso. Esta es nuestra fórmula: debe ser divertido, basarse en la comunicación y girar alrededor del mundo real.',
    'lena' => 'Doctora especialista en Ruso como lengua extrajera y cofundadora de Aprus.',

    'Learn Russian Online' => 'Aprende Ruso Online',
    'learn_russian_online_title_1' => '¿Qué encontraré?',
    'learn_russian_online_text_1' => 'Des del nivel principiante hasta el avanzado. Iremos incrementando la dificultad poco a poco haciendo que el proceso sea instructivo... ¡y divertido!',
    'learn_russian_online_title_2' => 'Feedback de profesores',
    'learn_russian_online_text_2' => 'Tendrás a tu disposición profesores reales especialistas en la enseñanza de Ruso como lengua extranjera que te acompañarán en cada paso.',
    'learn_russian_online_title_3' => '¿Cómo es una lección?',
    'learn_russian_online_text_3' => 'Pedir un taxi o comprar entradas para el teatro son actividades comunes que aprenderás con Aprus. Todo gira alrededor del mundo real.',
    'learn_russian_online_title_4' => 'A tu ritmo',
    'learn_russian_online_text_4' => 'Nuestro objetivo es que aprendas Ruso... ¡pero sin stress! Puedes incrementar tu nivel con solo 10 minutos al día.',

    'try_free' => 'Pruébalo Gratis',
];