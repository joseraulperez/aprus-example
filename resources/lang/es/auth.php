<?php

return [

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',


    /*
    |--------------------------------------------------------------------------
    | Recovery Password
    |--------------------------------------------------------------------------
    */
    'recovery.title' => '¿Olvidaste la contraseña?',
    'recovery.text' => 'No te preocupes. Escribe tu email aquí abajo y te enviaremos las instrucciones para cambiar tu contraseña.',
    'recovery.btn_send_reset' => 'Envíame las instrucciones',

    /*
    |--------------------------------------------------------------------------
    | Reset Password
    |--------------------------------------------------------------------------
    */
    'Reset Password' => 'Cambiar contraseña',
    'reset_password' => 'Escribe una nueva contraseña para tu cuenta.',
];
