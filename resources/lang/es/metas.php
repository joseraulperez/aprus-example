<?php

return [
    'home_title' => 'Aprende Ruso Divirtiéndote | Aprus',
    'home_description' => 'Prueba gratis nuestro curso online y aprende a leer, escribir, hablar y entender el Ruso con cientos de imágenes, audios y videos.',

    'blog_title' => 'Tu Dosis diaria de Ruso | Aprus',
    'blog_description' => 'En el blog de Aprus te enseñamos trucos para aprender en lenguaje Ruso y curiosidades sobre la cultura Rusa.',

    'privacy_policy_title' => 'Política de Privacidad | Aprus',
    'privacy_policy_description' => 'Política de Privacidad de Aprus',

    'terms_of_service_title' => 'Términos del Servicio | Aprus',
    'terms_of_service_description' => 'Términos del Servicio de Aprus',

    'contact_us_title' => 'Contacta con Nosotros | Aprus',
    'contact_us_description' => 'Contacta con Aprus',

    'about_us_title' => 'About Aprus',
    'about_us_description' => 'About Aprus',

    'russian_names_title' => 'Russian Names | Aprus',
    'russian_names_description' => 'Russian Names',

    'login.title' => 'Iniciar sesión en Aprus',
    'login.description' => 'Bienvenido de nuevo a Aprus.',

    'register.title' => 'Iniciar sesión en Aprus',
    'register.description' => 'Regístrate en Aprus. Prueba gratis nuestro curso online y aprende Ruso con cientos de imágenes, audios y videos.',

    'recovery.title' => 'Restablecimiento de contraseña',
    'recovery.description' => 'Restablecimiento de contraseña',

];