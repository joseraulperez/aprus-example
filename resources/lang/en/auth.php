<?php

return [

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',




    /*
    |--------------------------------------------------------------------------
    | Recovery Password
    |--------------------------------------------------------------------------
    */
    'recovery.title' => 'Forgot your password?',
    'recovery.text' => 'No worries. Enter your email address bellow and we\'ll send you the instructions to reset your password.',
    'recovery.btn_send_reset' => 'Send Me Reset Instructions',

    /*
    |--------------------------------------------------------------------------
    | Reset Password
    |--------------------------------------------------------------------------
    */
    'Reset Password' => 'Cambiar contraseña',
    'reset_password' => 'Escribe una nueva contraseña para tu cuenta.',
];
