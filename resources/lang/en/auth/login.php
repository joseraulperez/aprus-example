<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Email Verification
    |--------------------------------------------------------------------------
    */
    'email_verified' => 'Email verified successfully. You can access to your account now.',
    'error_verify_email' => 'You need to confirm your account. We have sent you an activation code, please check your email.',

    /*
    |--------------------------------------------------------------------------
    | Login
    |--------------------------------------------------------------------------
    */
    'title' => 'Welcome Back!',
    'use_social_network' => 'Continue with your favorite Social Network',
    'use_email' => 'Continue with your email',
    'sign_up' => 'Don\'t have account yet? <a href="' . route('register') .'">Sign up</a>',
    'forgot_password' => 'Forgot Your Password?',
    'btn_login' => 'Login',

];