<?php

return [
    'title' => 'Reset Password',
    'text' => 'Enter you email and the new password for your account.',

    'placeholder_password_confirm' => 'confirm password',
    'btn_reset' => 'Reset Password',
];