<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Register Success
    |--------------------------------------------------------------------------
    */
    '75% complete' => '75% complete',
    'You are almost there!' => 'You are almost there!',
    'verify_your_email' => 'We need to verify that you are a real person. For that, we have sent you a confirmation email. Please click in the confirm button in that email and start your adventure!<br><br>If you find you\'ve not received the email, please be sure to check your spam folder.',


    /*
    |--------------------------------------------------------------------------
    | Register
    |--------------------------------------------------------------------------
    */
    'title' => 'Learn Russian While Having Fun',
    'description' => 'Once you join Aprus, you will get access to our free Russian introductory course, exclusive tips, and much more!',
    'use_social_network' => 'You can join using your favorite Social Network',
    'use_email' => 'Or using your best email',
    'terms' => 'By signing up, you agree to the Terms of Service and Privacy Policy of Aprus.',

    'placeholder_your_name' => 'Your Name',
    'placeholder_your_email' => 'your@email.com',
    'placeholder_password' => 'password',
    'btn_join' => 'I Want to Join!',
    'login' => 'Have an account? <a href="' . route('login') .'">Log in</a>',

];