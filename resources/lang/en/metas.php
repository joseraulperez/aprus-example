<?php

return [
    'home_title' => 'Learn Russian While Having Fun | Aprus',
    'home_description' => 'Try our online course for free and learn to read, write, speak, and understand Russian with hundreds of images, audios, and videos.',

    'blog_title' => 'Your Daily Dose of Russian | Aprus',
    'blog_description' => 'The Aprus blog provides tips and tricks for learning the Russian language and shows you curious facts about Russian culture.',

    'privacy_policy_title' => 'Privacy Policy | Aprus',
    'privacy_policy_description' => 'Privacy Policy of Aprus',

    'terms_of_service_title' => 'Terms of Service | Aprus',
    'terms_of_service_description' => 'Terms of Service of Aprus',

    'contact_us_title' => 'Contact Us | Aprus',
    'contact_us_description' => 'Contact with Aprus',

    'about_us_title' => 'About Aprus',
    'about_us_description' => 'About Aprus',

    'russian_names_title' => 'Russian Names | Aprus',
    'russian_names_description' => 'Russian Names',

    'login.title' => 'Login on Aprus',
    'login.description' => 'Welcome back to Aprus.',

    'register.title' => 'Sign up for Aprus',
    'register.description' => 'Join Aprus today - it only takes a few seconds. Try our online course for free and learn Russian with hundreds of images, audios, and videos.',

    'recovery.title' => 'Password Reset',
    'recovery.description' => 'Password Reset',
];