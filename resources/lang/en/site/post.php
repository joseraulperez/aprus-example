<?php

return [
    'author' => 'by :first_name :last_name',
    'Continue reading' => 'Continue reading',
    'Try our Course Online Free!' => 'Try our Course Online Free!',
    'Learn Russian online having fun!' => 'Learn Russian online having fun!',
    'learn_russian_post_excerpt' => 'Do you want to learn Russian? We will explain you how!',

    // Banner
    'banner_title' => 'Learn Russian While Having Fun',
    'banner_text' => 'Try our Introductory course of Russian for Free',
];