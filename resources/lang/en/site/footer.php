<?php

return [
    'Home' => 'Home',
    'Blog' => 'Blog',
    'Contact Us' => 'Contact Us',
    'Terms of Use' => 'Terms of Use',
    'Privacy Policy' => 'Privacy Policy',
    'About Us' => 'About Us',
    'Aprus in other languages:' => 'Aprus in other languages:',
    'Log In' => 'Log In',
    'Sign Up' => 'Sign Up',
    'Pages' => 'Pages',
    'Company' => 'Company',

    'other_languages' => 'Aprus in'
];