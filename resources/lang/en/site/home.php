<?php
return [

    'title' => 'Learn Russian While Having Fun',
    'subtitle' => 'Try our online course for free and learn Russian with hundreds of images, audios, and videos.',

    'designed_by_teachers_text' => 'Years of experience with students around the world give us a unique vision on how to teach Russian most efficiently. This is our formula: it must be fun, based on communication, and real world exercises.',
    'lena' => 'Ph.D., Teacher of Russian as a foreign language and co-founder of Aprus.',

    'Learn Russian Online' => 'Learn Russian Online',
    'learn_russian_online_title_1' => 'What will I find?',
    'learn_russian_online_text_1' => 'From the start to the advanced level, we will increment the difficulty little by little, making the process instructive... and fun!',
    'learn_russian_online_title_2' => 'Feedback from teachers',
    'learn_russian_online_text_2' => 'You will have a real teacher specialized in teaching Russian who will correct your exercises and help you with every step.',
    'learn_russian_online_title_3' => 'What is a lesson like?',
    'learn_russian_online_text_3' => 'Asking for a taxi or buying tickets for a theater are just common activities that you will learn with Aprus. Everything resolves around the real world.',
    'learn_russian_online_title_4' => 'At your own pace',
    'learn_russian_online_text_4' => 'Our objective is that you really learn Russian... but stress-free! You can boost your level with just 10 minutes per day.',

    'try_free' => 'Try for free',
];