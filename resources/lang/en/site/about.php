<?php
return [
    'title' => 'About Us',
    'subtitle' => 'Settled in Barcelona (Spain), Aprus was born because we love to teach Russian. We offer face-to-face lessons, online courses, books... anything that helps people to know Russia and its culture.',

    'Founder' => 'Founder',
    'Founder-girl' => 'Founder',
    'contact_us_title' => 'Any Questions?',

    'who_is_behind_title' => 'Our Team',
    'lena_bio' => 'Elena has Ph.D. in Russian Philology, and she is a specialist in teaching Russian as a foreign language. She has been working with students from all over the world for over 10 years. She likes reading, travelling and dancing.',
    'raul_bio' => 'An engineer and an entrepreneur, Raul is an enthusiast of new technologies. He has worked in companies with huge volumes of users.',
    'btn_contact' => 'Contact Us',
];