<?php
return [
    'your@email.com' => 'your@email.com',
    'Your Name' => 'Your Name',
    'Message' => 'Message',
    'Get in Touch' => 'Get in Touch',
    'How can we help you?' => 'How can we help you?',
    'Send' => 'Send',
    'Message sent successfully' => 'Message sent successfully',
];