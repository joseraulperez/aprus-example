
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('Lesson', require('./components/Lesson.vue'));
Vue.component('Test', require('./components/Test.vue'));
Vue.component('BtnPlayAudio', require('./components/BtnPlayAudio.vue'));
Vue.component('Chat', require('./components/Chat.vue'));
Vue.component('VueVideo', require('./components/VueVideo.vue'));

const app = new Vue({
    el: '#app',
    data: {
        show_chat: false,
    },
    methods: {
        openChat() {
            this.show_chat = true;
        },
        closeChat() {
            this.show_chat = false;
        },
    },

    mounted() {
        this.$on('hide_chat', function () {
            this.closeChat();
        });
    }
});
