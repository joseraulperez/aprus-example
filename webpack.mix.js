let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.js('resources/assets/js/main.js', 'public/js')
//   .sass('resources/assets/sass/main.scss', 'public/css');

mix
    // App
    .js('resources/assets/app/js/main.js', 'public/js/app')
    .sass('resources/assets/app/sass/main.scss', 'public/css/app')

    // Site
//    .js('resources/assets/app/js/main.js', 'public/js')
    .sass('resources/assets/site/sass/main.scss', 'public/css/site')

    .version();
